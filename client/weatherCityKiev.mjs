export const weatherCityJson = 
{
	"coord": {
		"lon": 30.5167,
		"lat": 50.4333
	},
	"weather": [
		{
			"id": 500,
			"main": "Rain",
			"description": "light rain",
			"icon": "10n"
		}
	],
	"base": "stations",
	"main": {
		"temp": 277.69,
		"feels_like": 274.88,
		"temp_min": 276.48,
		"temp_max": 279.26,
		"pressure": 1016,
		"humidity": 93
	},
	"visibility": 6000,
	"wind": {
		"speed": 2,
		"deg": 290
	},
	"rain": {
		"1h": 0.24
	},
	"clouds": {
		"all": 90
	},
	"dt": 1614384407,
	"sys": {
		"type": 1,
		"id": 8903,
		"country": "UA",
		"sunrise": 1614401169,
		"sunset": 1614440131
	},
	"timezone": 7200,
	"id": 703448,
	"name": "Kyiv",
	"cod": 200
}
