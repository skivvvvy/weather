export const weatherForecastJson = 
{
	"lat": 50.4333,
	"lon": 30.5167,
	"timezone": "Europe/Kiev",
	"timezone_offset": 7200,
	"current": {
		"dt": 1614554140,
		"sunrise": 1614573720,
		"sunset": 1614613134,
		"temp": 274.06,
		"feels_like": 270.11,
		"pressure": 1021,
		"humidity": 100,
		"dew_point": 274.06,
		"uvi": 0,
		"clouds": 90,
		"visibility": 10000,
		"wind_speed": 3,
		"wind_deg": 360,
		"weather": [
			{
				"id": 804,
				"main": "Clouds",
				"description": "overcast clouds",
				"icon": "04n"
			}
		]
	},
	"minutely": [
		{
			"dt": 1614554160,
			"precipitation": 0
		},
		{
			"dt": 1614554220,
			"precipitation": 0
		},
		{
			"dt": 1614554280,
			"precipitation": 0
		},
		{
			"dt": 1614554340,
			"precipitation": 0
		},
		{
			"dt": 1614554400,
			"precipitation": 0
		},
		{
			"dt": 1614554460,
			"precipitation": 0
		},
		{
			"dt": 1614554520,
			"precipitation": 0
		},
		{
			"dt": 1614554580,
			"precipitation": 0
		},
		{
			"dt": 1614554640,
			"precipitation": 0
		},
		{
			"dt": 1614554700,
			"precipitation": 0
		},
		{
			"dt": 1614554760,
			"precipitation": 0
		},
		{
			"dt": 1614554820,
			"precipitation": 0
		},
		{
			"dt": 1614554880,
			"precipitation": 0
		},
		{
			"dt": 1614554940,
			"precipitation": 0
		},
		{
			"dt": 1614555000,
			"precipitation": 0
		},
		{
			"dt": 1614555060,
			"precipitation": 0
		},
		{
			"dt": 1614555120,
			"precipitation": 0
		},
		{
			"dt": 1614555180,
			"precipitation": 0
		},
		{
			"dt": 1614555240,
			"precipitation": 0
		},
		{
			"dt": 1614555300,
			"precipitation": 0
		},
		{
			"dt": 1614555360,
			"precipitation": 0
		},
		{
			"dt": 1614555420,
			"precipitation": 0
		},
		{
			"dt": 1614555480,
			"precipitation": 0
		},
		{
			"dt": 1614555540,
			"precipitation": 0
		},
		{
			"dt": 1614555600,
			"precipitation": 0
		},
		{
			"dt": 1614555660,
			"precipitation": 0
		},
		{
			"dt": 1614555720,
			"precipitation": 0
		},
		{
			"dt": 1614555780,
			"precipitation": 0
		},
		{
			"dt": 1614555840,
			"precipitation": 0
		},
		{
			"dt": 1614555900,
			"precipitation": 0
		},
		{
			"dt": 1614555960,
			"precipitation": 0
		},
		{
			"dt": 1614556020,
			"precipitation": 0
		},
		{
			"dt": 1614556080,
			"precipitation": 0
		},
		{
			"dt": 1614556140,
			"precipitation": 0
		},
		{
			"dt": 1614556200,
			"precipitation": 0
		},
		{
			"dt": 1614556260,
			"precipitation": 0
		},
		{
			"dt": 1614556320,
			"precipitation": 0
		},
		{
			"dt": 1614556380,
			"precipitation": 0
		},
		{
			"dt": 1614556440,
			"precipitation": 0
		},
		{
			"dt": 1614556500,
			"precipitation": 0
		},
		{
			"dt": 1614556560,
			"precipitation": 0
		},
		{
			"dt": 1614556620,
			"precipitation": 0
		},
		{
			"dt": 1614556680,
			"precipitation": 0
		},
		{
			"dt": 1614556740,
			"precipitation": 0
		},
		{
			"dt": 1614556800,
			"precipitation": 0
		},
		{
			"dt": 1614556860,
			"precipitation": 0
		},
		{
			"dt": 1614556920,
			"precipitation": 0
		},
		{
			"dt": 1614556980,
			"precipitation": 0
		},
		{
			"dt": 1614557040,
			"precipitation": 0
		},
		{
			"dt": 1614557100,
			"precipitation": 0
		},
		{
			"dt": 1614557160,
			"precipitation": 0
		},
		{
			"dt": 1614557220,
			"precipitation": 0
		},
		{
			"dt": 1614557280,
			"precipitation": 0
		},
		{
			"dt": 1614557340,
			"precipitation": 0
		},
		{
			"dt": 1614557400,
			"precipitation": 0
		},
		{
			"dt": 1614557460,
			"precipitation": 0
		},
		{
			"dt": 1614557520,
			"precipitation": 0
		},
		{
			"dt": 1614557580,
			"precipitation": 0
		},
		{
			"dt": 1614557640,
			"precipitation": 0
		},
		{
			"dt": 1614557700,
			"precipitation": 0
		},
		{
			"dt": 1614557760,
			"precipitation": 0
		}
	],
	"hourly": [
		{
			"dt": 1614553200,
			"temp": 274.06,
			"feels_like": 271.23,
			"pressure": 1021,
			"humidity": 100,
			"dew_point": 274.06,
			"uvi": 0,
			"clouds": 90,
			"visibility": 29,
			"wind_speed": 1.4,
			"wind_deg": 28,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0.74
		},
		{
			"dt": 1614556800,
			"temp": 274.01,
			"feels_like": 271.04,
			"pressure": 1022,
			"humidity": 99,
			"dew_point": 273.87,
			"uvi": 0,
			"clouds": 95,
			"visibility": 28,
			"wind_speed": 1.56,
			"wind_deg": 34,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0.74
		},
		{
			"dt": 1614560400,
			"temp": 273.9,
			"feels_like": 270.7,
			"pressure": 1023,
			"humidity": 98,
			"dew_point": 273.62,
			"uvi": 0,
			"clouds": 98,
			"visibility": 31,
			"wind_speed": 1.83,
			"wind_deg": 20,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0.34
		},
		{
			"dt": 1614564000,
			"temp": 273.81,
			"feels_like": 270.36,
			"pressure": 1024,
			"humidity": 97,
			"dew_point": 273.39,
			"uvi": 0,
			"clouds": 99,
			"visibility": 36,
			"wind_speed": 2.14,
			"wind_deg": 8,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0.26
		},
		{
			"dt": 1614567600,
			"temp": 273.72,
			"feels_like": 269.95,
			"pressure": 1025,
			"humidity": 97,
			"dew_point": 273.3,
			"uvi": 0,
			"clouds": 100,
			"visibility": 37,
			"wind_speed": 2.58,
			"wind_deg": 4,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0.26
		},
		{
			"dt": 1614571200,
			"temp": 273.69,
			"feels_like": 269.76,
			"pressure": 1026,
			"humidity": 97,
			"dew_point": 273.34,
			"uvi": 0,
			"clouds": 100,
			"visibility": 36,
			"wind_speed": 2.8,
			"wind_deg": 2,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0.18
		},
		{
			"dt": 1614574800,
			"temp": 273.67,
			"feels_like": 269.73,
			"pressure": 1027,
			"humidity": 96,
			"dew_point": 273.24,
			"uvi": 0,
			"clouds": 100,
			"visibility": 81,
			"wind_speed": 2.79,
			"wind_deg": 4,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0.14
		},
		{
			"dt": 1614578400,
			"temp": 273.72,
			"feels_like": 269.43,
			"pressure": 1027,
			"humidity": 95,
			"dew_point": 273.13,
			"uvi": 0.26,
			"clouds": 100,
			"visibility": 231,
			"wind_speed": 3.27,
			"wind_deg": 2,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0.1
		},
		{
			"dt": 1614582000,
			"temp": 273.89,
			"feels_like": 269.23,
			"pressure": 1028,
			"humidity": 94,
			"dew_point": 273.17,
			"uvi": 0.21,
			"clouds": 100,
			"visibility": 4165,
			"wind_speed": 3.8,
			"wind_deg": 3,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614585600,
			"temp": 274.13,
			"feels_like": 269.49,
			"pressure": 1029,
			"humidity": 92,
			"dew_point": 273.05,
			"uvi": 0.36,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 3.76,
			"wind_deg": 3,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614589200,
			"temp": 274.3,
			"feels_like": 269.86,
			"pressure": 1030,
			"humidity": 92,
			"dew_point": 273.27,
			"uvi": 0.48,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 3.5,
			"wind_deg": 358,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0.04
		},
		{
			"dt": 1614592800,
			"temp": 274.44,
			"feels_like": 270.45,
			"pressure": 1030,
			"humidity": 94,
			"dew_point": 273.71,
			"uvi": 0.55,
			"clouds": 100,
			"visibility": 4789,
			"wind_speed": 2.96,
			"wind_deg": 346,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614596400,
			"temp": 274.78,
			"feels_like": 270.79,
			"pressure": 1030,
			"humidity": 92,
			"dew_point": 273.72,
			"uvi": 0.49,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 2.96,
			"wind_deg": 339,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614600000,
			"temp": 275.27,
			"feels_like": 271.38,
			"pressure": 1030,
			"humidity": 87,
			"dew_point": 273.33,
			"uvi": 0.36,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 2.76,
			"wind_deg": 339,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614603600,
			"temp": 275.99,
			"feels_like": 272.22,
			"pressure": 1030,
			"humidity": 80,
			"dew_point": 272.69,
			"uvi": 0.43,
			"clouds": 97,
			"visibility": 10000,
			"wind_speed": 2.49,
			"wind_deg": 339,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614607200,
			"temp": 276.14,
			"feels_like": 272.41,
			"pressure": 1030,
			"humidity": 79,
			"dew_point": 272.55,
			"uvi": 0.18,
			"clouds": 96,
			"visibility": 10000,
			"wind_speed": 2.43,
			"wind_deg": 327,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614610800,
			"temp": 275.66,
			"feels_like": 271.91,
			"pressure": 1030,
			"humidity": 82,
			"dew_point": 272.54,
			"uvi": 0,
			"clouds": 97,
			"visibility": 10000,
			"wind_speed": 2.47,
			"wind_deg": 309,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614614400,
			"temp": 275.32,
			"feels_like": 271.21,
			"pressure": 1031,
			"humidity": 84,
			"dew_point": 272.21,
			"uvi": 0,
			"clouds": 98,
			"visibility": 10000,
			"wind_speed": 2.99,
			"wind_deg": 299,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614618000,
			"temp": 275.17,
			"feels_like": 270.92,
			"pressure": 1032,
			"humidity": 84,
			"dew_point": 271.96,
			"uvi": 0,
			"clouds": 98,
			"visibility": 10000,
			"wind_speed": 3.15,
			"wind_deg": 298,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614621600,
			"temp": 275.03,
			"feels_like": 271.04,
			"pressure": 1032,
			"humidity": 85,
			"dew_point": 271.93,
			"uvi": 0,
			"clouds": 98,
			"visibility": 10000,
			"wind_speed": 2.79,
			"wind_deg": 292,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614625200,
			"temp": 274.97,
			"feels_like": 270.91,
			"pressure": 1031,
			"humidity": 85,
			"dew_point": 271.88,
			"uvi": 0,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 2.88,
			"wind_deg": 283,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614628800,
			"temp": 274.32,
			"feels_like": 270.28,
			"pressure": 1031,
			"humidity": 89,
			"dew_point": 271.76,
			"uvi": 0,
			"clouds": 93,
			"visibility": 10000,
			"wind_speed": 2.84,
			"wind_deg": 280,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614632400,
			"temp": 273.89,
			"feels_like": 269.9,
			"pressure": 1031,
			"humidity": 92,
			"dew_point": 271.52,
			"uvi": 0,
			"clouds": 83,
			"visibility": 10000,
			"wind_speed": 2.78,
			"wind_deg": 270,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614636000,
			"temp": 273.86,
			"feels_like": 269.75,
			"pressure": 1031,
			"humidity": 92,
			"dew_point": 271.53,
			"uvi": 0,
			"clouds": 75,
			"visibility": 10000,
			"wind_speed": 2.94,
			"wind_deg": 264,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614639600,
			"temp": 274.05,
			"feels_like": 269.47,
			"pressure": 1031,
			"humidity": 91,
			"dew_point": 271.68,
			"uvi": 0,
			"clouds": 77,
			"visibility": 10000,
			"wind_speed": 3.63,
			"wind_deg": 261,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614643200,
			"temp": 273.9,
			"feels_like": 269.06,
			"pressure": 1031,
			"humidity": 92,
			"dew_point": 271.91,
			"uvi": 0,
			"clouds": 75,
			"visibility": 10000,
			"wind_speed": 3.99,
			"wind_deg": 259,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614646800,
			"temp": 273.95,
			"feels_like": 268.77,
			"pressure": 1029,
			"humidity": 92,
			"dew_point": 272.1,
			"uvi": 0,
			"clouds": 40,
			"visibility": 10000,
			"wind_speed": 4.49,
			"wind_deg": 258,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614650400,
			"temp": 274.11,
			"feels_like": 268.67,
			"pressure": 1028,
			"humidity": 91,
			"dew_point": 272.26,
			"uvi": 0,
			"clouds": 70,
			"visibility": 10000,
			"wind_speed": 4.86,
			"wind_deg": 261,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614654000,
			"temp": 274.14,
			"feels_like": 268.6,
			"pressure": 1028,
			"humidity": 91,
			"dew_point": 272.36,
			"uvi": 0,
			"clouds": 80,
			"visibility": 10000,
			"wind_speed": 5.02,
			"wind_deg": 264,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614657600,
			"temp": 274.14,
			"feels_like": 268.32,
			"pressure": 1027,
			"humidity": 91,
			"dew_point": 272.43,
			"uvi": 0,
			"clouds": 85,
			"visibility": 10000,
			"wind_speed": 5.42,
			"wind_deg": 264,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614661200,
			"temp": 274.27,
			"feels_like": 268.31,
			"pressure": 1026,
			"humidity": 91,
			"dew_point": 272.72,
			"uvi": 0,
			"clouds": 88,
			"visibility": 10000,
			"wind_speed": 5.64,
			"wind_deg": 266,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614664800,
			"temp": 274.95,
			"feels_like": 268.94,
			"pressure": 1026,
			"humidity": 88,
			"dew_point": 273.21,
			"uvi": 0.32,
			"clouds": 90,
			"visibility": 10000,
			"wind_speed": 5.76,
			"wind_deg": 265,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614668400,
			"temp": 276.03,
			"feels_like": 269.88,
			"pressure": 1025,
			"humidity": 84,
			"dew_point": 273.73,
			"uvi": 0.68,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 6.05,
			"wind_deg": 267,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614672000,
			"temp": 277.42,
			"feels_like": 271.12,
			"pressure": 1024,
			"humidity": 78,
			"dew_point": 273.96,
			"uvi": 1.18,
			"clouds": 94,
			"visibility": 10000,
			"wind_speed": 6.33,
			"wind_deg": 274,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614675600,
			"temp": 278.43,
			"feels_like": 272.01,
			"pressure": 1023,
			"humidity": 73,
			"dew_point": 274.17,
			"uvi": 1.58,
			"clouds": 95,
			"visibility": 10000,
			"wind_speed": 6.51,
			"wind_deg": 282,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614679200,
			"temp": 279.2,
			"feels_like": 272.67,
			"pressure": 1023,
			"humidity": 69,
			"dew_point": 274.06,
			"uvi": 1.92,
			"clouds": 97,
			"visibility": 10000,
			"wind_speed": 6.66,
			"wind_deg": 285,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614682800,
			"temp": 279.85,
			"feels_like": 273.24,
			"pressure": 1022,
			"humidity": 66,
			"dew_point": 274.11,
			"uvi": 1.72,
			"clouds": 97,
			"visibility": 10000,
			"wind_speed": 6.78,
			"wind_deg": 289,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614686400,
			"temp": 280.32,
			"feels_like": 273.84,
			"pressure": 1021,
			"humidity": 65,
			"dew_point": 274.27,
			"uvi": 1.26,
			"clouds": 86,
			"visibility": 10000,
			"wind_speed": 6.65,
			"wind_deg": 292,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614690000,
			"temp": 280.4,
			"feels_like": 273.98,
			"pressure": 1021,
			"humidity": 65,
			"dew_point": 274.44,
			"uvi": 0.71,
			"clouds": 6,
			"visibility": 10000,
			"wind_speed": 6.58,
			"wind_deg": 297,
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614693600,
			"temp": 280.04,
			"feels_like": 273.87,
			"pressure": 1020,
			"humidity": 67,
			"dew_point": 274.48,
			"uvi": 0.29,
			"clouds": 20,
			"visibility": 10000,
			"wind_speed": 6.24,
			"wind_deg": 302,
			"weather": [
				{
					"id": 801,
					"main": "Clouds",
					"description": "few clouds",
					"icon": "02d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614697200,
			"temp": 279.13,
			"feels_like": 273.55,
			"pressure": 1020,
			"humidity": 72,
			"dew_point": 274.51,
			"uvi": 0,
			"clouds": 27,
			"visibility": 10000,
			"wind_speed": 5.43,
			"wind_deg": 310,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614700800,
			"temp": 278.15,
			"feels_like": 272.49,
			"pressure": 1020,
			"humidity": 75,
			"dew_point": 274.25,
			"uvi": 0,
			"clouds": 36,
			"visibility": 10000,
			"wind_speed": 5.45,
			"wind_deg": 317,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614704400,
			"temp": 278.19,
			"feels_like": 271.5,
			"pressure": 1020,
			"humidity": 74,
			"dew_point": 273.96,
			"uvi": 0,
			"clouds": 49,
			"visibility": 10000,
			"wind_speed": 6.89,
			"wind_deg": 319,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614708000,
			"temp": 277.08,
			"feels_like": 269.21,
			"pressure": 1021,
			"humidity": 75,
			"dew_point": 273.14,
			"uvi": 0,
			"clouds": 57,
			"visibility": 10000,
			"wind_speed": 8.39,
			"wind_deg": 328,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614711600,
			"temp": 276.85,
			"feels_like": 269.18,
			"pressure": 1021,
			"humidity": 76,
			"dew_point": 272.88,
			"uvi": 0,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 8.1,
			"wind_deg": 335,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614715200,
			"temp": 276.75,
			"feels_like": 269.42,
			"pressure": 1022,
			"humidity": 77,
			"dew_point": 273.17,
			"uvi": 0,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 7.62,
			"wind_deg": 342,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614718800,
			"temp": 277.02,
			"feels_like": 270.48,
			"pressure": 1022,
			"humidity": 74,
			"dew_point": 272.33,
			"uvi": 0,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 6.44,
			"wind_deg": 338,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1614722400,
			"temp": 277,
			"feels_like": 270.76,
			"pressure": 1023,
			"humidity": 74,
			"dew_point": 271.79,
			"uvi": 0,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 6,
			"wind_deg": 337,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		}
	],
	"daily": [
		{
			"dt": 1614592800,
			"sunrise": 1614573720,
			"sunset": 1614613134,
			"temp": {
				"day": 274.44,
				"min": 273.67,
				"max": 276.14,
				"night": 273.89,
				"eve": 275.32,
				"morn": 273.69
			},
			"feels_like": {
				"day": 270.45,
				"night": 269.9,
				"eve": 271.21,
				"morn": 269.76
			},
			"pressure": 1030,
			"humidity": 94,
			"dew_point": 273.71,
			"wind_speed": 2.96,
			"wind_deg": 346,
			"weather": [
				{
					"id": 600,
					"main": "Snow",
					"description": "light snow",
					"icon": "13d"
				}
			],
			"clouds": 100,
			"pop": 0.97,
			"snow": 0.1,
			"uvi": 0.55
		},
		{
			"dt": 1614679200,
			"sunrise": 1614659994,
			"sunset": 1614699635,
			"temp": {
				"day": 279.2,
				"min": 273.86,
				"max": 280.4,
				"night": 277.02,
				"eve": 278.15,
				"morn": 274.14
			},
			"feels_like": {
				"day": 272.67,
				"night": 270.48,
				"eve": 272.49,
				"morn": 268.32
			},
			"pressure": 1023,
			"humidity": 69,
			"dew_point": 274.06,
			"wind_speed": 6.66,
			"wind_deg": 285,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"clouds": 97,
			"pop": 0,
			"uvi": 1.92
		},
		{
			"dt": 1614765600,
			"sunrise": 1614746268,
			"sunset": 1614786136,
			"temp": {
				"day": 277.37,
				"min": 273.22,
				"max": 278.68,
				"night": 274.1,
				"eve": 278.08,
				"morn": 273.4
			},
			"feels_like": {
				"day": 272.48,
				"night": 270.05,
				"eve": 274.04,
				"morn": 268.31
			},
			"pressure": 1028,
			"humidity": 70,
			"dew_point": 270.67,
			"wind_speed": 3.99,
			"wind_deg": 326,
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01d"
				}
			],
			"clouds": 1,
			"pop": 0,
			"uvi": 1.97
		},
		{
			"dt": 1614852000,
			"sunrise": 1614832541,
			"sunset": 1614872636,
			"temp": {
				"day": 279.52,
				"min": 273.63,
				"max": 282.93,
				"night": 277.72,
				"eve": 281.59,
				"morn": 273.71
			},
			"feels_like": {
				"day": 273.29,
				"night": 273.14,
				"eve": 276.39,
				"morn": 268.85
			},
			"pressure": 1015,
			"humidity": 62,
			"dew_point": 272.24,
			"wind_speed": 5.99,
			"wind_deg": 236,
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01d"
				}
			],
			"clouds": 2,
			"pop": 0,
			"uvi": 2.18
		},
		{
			"dt": 1614938400,
			"sunrise": 1614918814,
			"sunset": 1614959136,
			"temp": {
				"day": 276.55,
				"min": 273.73,
				"max": 277.27,
				"night": 273.73,
				"eve": 276.49,
				"morn": 276.69
			},
			"feels_like": {
				"day": 273.58,
				"night": 268.25,
				"eve": 274.03,
				"morn": 272.93
			},
			"pressure": 1008,
			"humidity": 95,
			"dew_point": 275.94,
			"wind_speed": 2.02,
			"wind_deg": 227,
			"weather": [
				{
					"id": 616,
					"main": "Snow",
					"description": "rain and snow",
					"icon": "13d"
				}
			],
			"clouds": 100,
			"pop": 1,
			"rain": 9.08,
			"snow": 1.13,
			"uvi": 0.39
		},
		{
			"dt": 1615024800,
			"sunrise": 1615005085,
			"sunset": 1615045636,
			"temp": {
				"day": 272.29,
				"min": 270.18,
				"max": 272.76,
				"night": 270.18,
				"eve": 272.08,
				"morn": 270.88
			},
			"feels_like": {
				"day": 265.22,
				"night": 262.89,
				"eve": 264.79,
				"morn": 264.18
			},
			"pressure": 1017,
			"humidity": 84,
			"dew_point": 263.88,
			"wind_speed": 6.66,
			"wind_deg": 317,
			"weather": [
				{
					"id": 600,
					"main": "Snow",
					"description": "light snow",
					"icon": "13d"
				}
			],
			"clouds": 66,
			"pop": 0.87,
			"snow": 1.37,
			"uvi": 1
		},
		{
			"dt": 1615111200,
			"sunrise": 1615091356,
			"sunset": 1615132135,
			"temp": {
				"day": 271.87,
				"min": 269.33,
				"max": 273.88,
				"night": 272.1,
				"eve": 273.63,
				"morn": 269.71
			},
			"feels_like": {
				"day": 266.12,
				"night": 266.62,
				"eve": 268.24,
				"morn": 263.15
			},
			"pressure": 1031,
			"humidity": 77,
			"dew_point": 259.82,
			"wind_speed": 4.52,
			"wind_deg": 308,
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01d"
				}
			],
			"clouds": 0,
			"pop": 0.3,
			"uvi": 1
		},
		{
			"dt": 1615197600,
			"sunrise": 1615177627,
			"sunset": 1615218634,
			"temp": {
				"day": 275.04,
				"min": 271.42,
				"max": 276.82,
				"night": 273.09,
				"eve": 275.55,
				"morn": 271.42
			},
			"feels_like": {
				"day": 268.7,
				"night": 267.75,
				"eve": 270.27,
				"morn": 265.77
			},
			"pressure": 1023,
			"humidity": 71,
			"dew_point": 263.12,
			"wind_speed": 5.68,
			"wind_deg": 257,
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01d"
				}
			],
			"clouds": 0,
			"pop": 0,
			"uvi": 1
		}
	]
}