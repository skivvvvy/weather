export const weatherForecastJson = 
{
	"lat": 51.5085,
	"lon": -0.1257,
	"timezone": "Europe/London",
	"timezone_offset": 3600,
	"current": {
		"dt": 1634782484,
		"sunrise": 1634798083,
		"sunset": 1634835312,
		"temp": 282.01,
		"feels_like": 281.68,
		"pressure": 995,
		"humidity": 93,
		"dew_point": 280.94,
		"uvi": 0,
		"clouds": 100,
		"visibility": 10000,
		"wind_speed": 1.34,
		"wind_deg": 279,
		"wind_gust": 5.36,
		"weather": [
			{
				"id": 804,
				"main": "Clouds",
				"description": "overcast clouds",
				"icon": "04n"
			}
		]
	},
	"minutely": [
		{
			"dt": 1634782500,
			"precipitation": 0
		},
		{
			"dt": 1634782560,
			"precipitation": 0
		},
		{
			"dt": 1634782620,
			"precipitation": 0
		},
		{
			"dt": 1634782680,
			"precipitation": 0
		},
		{
			"dt": 1634782740,
			"precipitation": 0
		},
		{
			"dt": 1634782800,
			"precipitation": 0
		},
		{
			"dt": 1634782860,
			"precipitation": 0
		},
		{
			"dt": 1634782920,
			"precipitation": 0
		},
		{
			"dt": 1634782980,
			"precipitation": 0
		},
		{
			"dt": 1634783040,
			"precipitation": 0
		},
		{
			"dt": 1634783100,
			"precipitation": 0
		},
		{
			"dt": 1634783160,
			"precipitation": 0
		},
		{
			"dt": 1634783220,
			"precipitation": 0
		},
		{
			"dt": 1634783280,
			"precipitation": 0
		},
		{
			"dt": 1634783340,
			"precipitation": 0
		},
		{
			"dt": 1634783400,
			"precipitation": 0
		},
		{
			"dt": 1634783460,
			"precipitation": 0
		},
		{
			"dt": 1634783520,
			"precipitation": 0
		},
		{
			"dt": 1634783580,
			"precipitation": 0
		},
		{
			"dt": 1634783640,
			"precipitation": 0
		},
		{
			"dt": 1634783700,
			"precipitation": 0
		},
		{
			"dt": 1634783760,
			"precipitation": 0
		},
		{
			"dt": 1634783820,
			"precipitation": 0
		},
		{
			"dt": 1634783880,
			"precipitation": 0
		},
		{
			"dt": 1634783940,
			"precipitation": 0
		},
		{
			"dt": 1634784000,
			"precipitation": 0
		},
		{
			"dt": 1634784060,
			"precipitation": 0
		},
		{
			"dt": 1634784120,
			"precipitation": 0
		},
		{
			"dt": 1634784180,
			"precipitation": 0
		},
		{
			"dt": 1634784240,
			"precipitation": 0
		},
		{
			"dt": 1634784300,
			"precipitation": 0
		},
		{
			"dt": 1634784360,
			"precipitation": 0
		},
		{
			"dt": 1634784420,
			"precipitation": 0
		},
		{
			"dt": 1634784480,
			"precipitation": 0
		},
		{
			"dt": 1634784540,
			"precipitation": 0
		},
		{
			"dt": 1634784600,
			"precipitation": 0
		},
		{
			"dt": 1634784660,
			"precipitation": 0
		},
		{
			"dt": 1634784720,
			"precipitation": 0
		},
		{
			"dt": 1634784780,
			"precipitation": 0
		},
		{
			"dt": 1634784840,
			"precipitation": 0
		},
		{
			"dt": 1634784900,
			"precipitation": 0
		},
		{
			"dt": 1634784960,
			"precipitation": 0
		},
		{
			"dt": 1634785020,
			"precipitation": 0
		},
		{
			"dt": 1634785080,
			"precipitation": 0
		},
		{
			"dt": 1634785140,
			"precipitation": 0
		},
		{
			"dt": 1634785200,
			"precipitation": 0
		},
		{
			"dt": 1634785260,
			"precipitation": 0
		},
		{
			"dt": 1634785320,
			"precipitation": 0
		},
		{
			"dt": 1634785380,
			"precipitation": 0
		},
		{
			"dt": 1634785440,
			"precipitation": 0
		},
		{
			"dt": 1634785500,
			"precipitation": 0
		},
		{
			"dt": 1634785560,
			"precipitation": 0
		},
		{
			"dt": 1634785620,
			"precipitation": 0
		},
		{
			"dt": 1634785680,
			"precipitation": 0
		},
		{
			"dt": 1634785740,
			"precipitation": 0
		},
		{
			"dt": 1634785800,
			"precipitation": 0
		},
		{
			"dt": 1634785860,
			"precipitation": 0
		},
		{
			"dt": 1634785920,
			"precipitation": 0
		},
		{
			"dt": 1634785980,
			"precipitation": 0
		},
		{
			"dt": 1634786040,
			"precipitation": 0
		},
		{
			"dt": 1634786100,
			"precipitation": 0
		}
	],
	"hourly": [
		{
			"dt": 1634781600,
			"temp": 282.01,
			"feels_like": 278.74,
			"pressure": 995,
			"humidity": 93,
			"dew_point": 280.94,
			"uvi": 0,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 6.63,
			"wind_deg": 299,
			"wind_gust": 12.92,
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10n"
				}
			],
			"pop": 0.94,
			"rain": {
				"1h": 0.16
			}
		},
		{
			"dt": 1634785200,
			"temp": 282.27,
			"feels_like": 278.94,
			"pressure": 995,
			"humidity": 89,
			"dew_point": 280.55,
			"uvi": 0,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 7.07,
			"wind_deg": 281,
			"wind_gust": 14.19,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0.84
		},
		{
			"dt": 1634788800,
			"temp": 282.32,
			"feels_like": 278.91,
			"pressure": 997,
			"humidity": 87,
			"dew_point": 280.27,
			"uvi": 0,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 7.4,
			"wind_deg": 271,
			"wind_gust": 15.13,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0.81
		},
		{
			"dt": 1634792400,
			"temp": 282.3,
			"feels_like": 278.86,
			"pressure": 998,
			"humidity": 82,
			"dew_point": 279.39,
			"uvi": 0,
			"clouds": 96,
			"visibility": 10000,
			"wind_speed": 7.46,
			"wind_deg": 264,
			"wind_gust": 14.96,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0.77
		},
		{
			"dt": 1634796000,
			"temp": 282.73,
			"feels_like": 279.3,
			"pressure": 1001,
			"humidity": 80,
			"dew_point": 279.45,
			"uvi": 0,
			"clouds": 95,
			"visibility": 10000,
			"wind_speed": 7.89,
			"wind_deg": 281,
			"wind_gust": 14.51,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0.75
		},
		{
			"dt": 1634799600,
			"temp": 281.35,
			"feels_like": 277.73,
			"pressure": 1004,
			"humidity": 69,
			"dew_point": 276,
			"uvi": 0,
			"clouds": 35,
			"visibility": 10000,
			"wind_speed": 7.16,
			"wind_deg": 288,
			"wind_gust": 13.79,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03d"
				}
			],
			"pop": 0.01
		},
		{
			"dt": 1634803200,
			"temp": 280.56,
			"feels_like": 276.68,
			"pressure": 1006,
			"humidity": 72,
			"dew_point": 275.67,
			"uvi": 0.14,
			"clouds": 23,
			"visibility": 10000,
			"wind_speed": 7.25,
			"wind_deg": 297,
			"wind_gust": 11.7,
			"weather": [
				{
					"id": 801,
					"main": "Clouds",
					"description": "few clouds",
					"icon": "02d"
				}
			],
			"pop": 0.19
		},
		{
			"dt": 1634806800,
			"temp": 279.88,
			"feels_like": 276.38,
			"pressure": 1009,
			"humidity": 85,
			"dew_point": 277.44,
			"uvi": 0.4,
			"clouds": 49,
			"visibility": 10000,
			"wind_speed": 5.64,
			"wind_deg": 320,
			"wind_gust": 9.82,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03d"
				}
			],
			"pop": 0.15
		},
		{
			"dt": 1634810400,
			"temp": 280.76,
			"feels_like": 277.75,
			"pressure": 1010,
			"humidity": 66,
			"dew_point": 274.78,
			"uvi": 0.99,
			"clouds": 60,
			"visibility": 10000,
			"wind_speed": 5.01,
			"wind_deg": 324,
			"wind_gust": 9.93,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04d"
				}
			],
			"pop": 0.1
		},
		{
			"dt": 1634814000,
			"temp": 282.33,
			"feels_like": 279.61,
			"pressure": 1011,
			"humidity": 55,
			"dew_point": 273.65,
			"uvi": 1.4,
			"clouds": 49,
			"visibility": 10000,
			"wind_speed": 5.27,
			"wind_deg": 329,
			"wind_gust": 7.66,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03d"
				}
			],
			"pop": 0.06
		},
		{
			"dt": 1634817600,
			"temp": 282.96,
			"feels_like": 280.68,
			"pressure": 1012,
			"humidity": 52,
			"dew_point": 273.45,
			"uvi": 1.57,
			"clouds": 42,
			"visibility": 10000,
			"wind_speed": 4.54,
			"wind_deg": 338,
			"wind_gust": 6.9,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03d"
				}
			],
			"pop": 0.06
		},
		{
			"dt": 1634821200,
			"temp": 283.61,
			"feels_like": 281.96,
			"pressure": 1013,
			"humidity": 48,
			"dew_point": 273.11,
			"uvi": 1.44,
			"clouds": 5,
			"visibility": 10000,
			"wind_speed": 4.29,
			"wind_deg": 337,
			"wind_gust": 6.49,
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634824800,
			"temp": 283.88,
			"feels_like": 282.24,
			"pressure": 1014,
			"humidity": 47,
			"dew_point": 272.91,
			"uvi": 1.04,
			"clouds": 4,
			"visibility": 10000,
			"wind_speed": 3.99,
			"wind_deg": 335,
			"wind_gust": 5.95,
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634828400,
			"temp": 283.82,
			"feels_like": 282.14,
			"pressure": 1015,
			"humidity": 46,
			"dew_point": 272.79,
			"uvi": 0.55,
			"clouds": 5,
			"visibility": 10000,
			"wind_speed": 3.78,
			"wind_deg": 326,
			"wind_gust": 5.69,
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634832000,
			"temp": 283.58,
			"feels_like": 281.93,
			"pressure": 1016,
			"humidity": 48,
			"dew_point": 272.93,
			"uvi": 0.2,
			"clouds": 19,
			"visibility": 10000,
			"wind_speed": 3.07,
			"wind_deg": 316,
			"wind_gust": 5.51,
			"weather": [
				{
					"id": 801,
					"main": "Clouds",
					"description": "few clouds",
					"icon": "02d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634835600,
			"temp": 282.76,
			"feels_like": 281.24,
			"pressure": 1017,
			"humidity": 50,
			"dew_point": 272.92,
			"uvi": 0,
			"clouds": 18,
			"visibility": 10000,
			"wind_speed": 2.92,
			"wind_deg": 309,
			"wind_gust": 6.32,
			"weather": [
				{
					"id": 801,
					"main": "Clouds",
					"description": "few clouds",
					"icon": "02n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634839200,
			"temp": 282.34,
			"feels_like": 280.86,
			"pressure": 1018,
			"humidity": 51,
			"dew_point": 272.76,
			"uvi": 0,
			"clouds": 17,
			"visibility": 10000,
			"wind_speed": 2.72,
			"wind_deg": 310,
			"wind_gust": 7.06,
			"weather": [
				{
					"id": 801,
					"main": "Clouds",
					"description": "few clouds",
					"icon": "02n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634842800,
			"temp": 281.98,
			"feels_like": 280.72,
			"pressure": 1018,
			"humidity": 53,
			"dew_point": 272.79,
			"uvi": 0,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 2.32,
			"wind_deg": 287,
			"wind_gust": 7.11,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634846400,
			"temp": 281.56,
			"feels_like": 279.93,
			"pressure": 1019,
			"humidity": 55,
			"dew_point": 272.99,
			"uvi": 0,
			"clouds": 82,
			"visibility": 10000,
			"wind_speed": 2.73,
			"wind_deg": 277,
			"wind_gust": 7.69,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634850000,
			"temp": 281.25,
			"feels_like": 279.46,
			"pressure": 1019,
			"humidity": 56,
			"dew_point": 272.98,
			"uvi": 0,
			"clouds": 55,
			"visibility": 10000,
			"wind_speed": 2.87,
			"wind_deg": 272,
			"wind_gust": 8.17,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634853600,
			"temp": 280.99,
			"feels_like": 279.03,
			"pressure": 1019,
			"humidity": 56,
			"dew_point": 272.83,
			"uvi": 0,
			"clouds": 42,
			"visibility": 10000,
			"wind_speed": 3.06,
			"wind_deg": 268,
			"wind_gust": 9.28,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634857200,
			"temp": 280.82,
			"feels_like": 278.54,
			"pressure": 1020,
			"humidity": 57,
			"dew_point": 272.84,
			"uvi": 0,
			"clouds": 34,
			"visibility": 10000,
			"wind_speed": 3.53,
			"wind_deg": 264,
			"wind_gust": 10.26,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634860800,
			"temp": 280.64,
			"feels_like": 278.21,
			"pressure": 1020,
			"humidity": 59,
			"dew_point": 273.2,
			"uvi": 0,
			"clouds": 29,
			"visibility": 10000,
			"wind_speed": 3.73,
			"wind_deg": 261,
			"wind_gust": 10.71,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634864400,
			"temp": 280.46,
			"feels_like": 277.96,
			"pressure": 1020,
			"humidity": 63,
			"dew_point": 273.88,
			"uvi": 0,
			"clouds": 0,
			"visibility": 10000,
			"wind_speed": 3.79,
			"wind_deg": 258,
			"wind_gust": 11.15,
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634868000,
			"temp": 280.38,
			"feels_like": 277.77,
			"pressure": 1020,
			"humidity": 66,
			"dew_point": 274.44,
			"uvi": 0,
			"clouds": 1,
			"visibility": 10000,
			"wind_speed": 3.96,
			"wind_deg": 260,
			"wind_gust": 11.57,
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634871600,
			"temp": 280.38,
			"feels_like": 277.68,
			"pressure": 1020,
			"humidity": 68,
			"dew_point": 274.95,
			"uvi": 0,
			"clouds": 3,
			"visibility": 10000,
			"wind_speed": 4.14,
			"wind_deg": 263,
			"wind_gust": 11.91,
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634875200,
			"temp": 280.31,
			"feels_like": 277.46,
			"pressure": 1020,
			"humidity": 71,
			"dew_point": 275.28,
			"uvi": 0,
			"clouds": 4,
			"visibility": 10000,
			"wind_speed": 4.4,
			"wind_deg": 262,
			"wind_gust": 12.06,
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634878800,
			"temp": 280.28,
			"feels_like": 277.4,
			"pressure": 1020,
			"humidity": 74,
			"dew_point": 275.95,
			"uvi": 0,
			"clouds": 4,
			"visibility": 10000,
			"wind_speed": 4.46,
			"wind_deg": 268,
			"wind_gust": 12.06,
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634882400,
			"temp": 280.29,
			"feels_like": 277.41,
			"pressure": 1020,
			"humidity": 75,
			"dew_point": 276.03,
			"uvi": 0,
			"clouds": 5,
			"visibility": 10000,
			"wind_speed": 4.45,
			"wind_deg": 266,
			"wind_gust": 11.93,
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634886000,
			"temp": 280.31,
			"feels_like": 277.49,
			"pressure": 1020,
			"humidity": 75,
			"dew_point": 276.12,
			"uvi": 0,
			"clouds": 26,
			"visibility": 10000,
			"wind_speed": 4.34,
			"wind_deg": 265,
			"wind_gust": 11.7,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634889600,
			"temp": 281.3,
			"feels_like": 278.69,
			"pressure": 1021,
			"humidity": 70,
			"dew_point": 276.05,
			"uvi": 0.09,
			"clouds": 63,
			"visibility": 10000,
			"wind_speed": 4.4,
			"wind_deg": 256,
			"wind_gust": 11.91,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634893200,
			"temp": 281.89,
			"feels_like": 279.32,
			"pressure": 1021,
			"humidity": 68,
			"dew_point": 276.19,
			"uvi": 0.26,
			"clouds": 75,
			"visibility": 10000,
			"wind_speed": 4.63,
			"wind_deg": 257,
			"wind_gust": 11.51,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634896800,
			"temp": 282.32,
			"feels_like": 279.72,
			"pressure": 1021,
			"humidity": 67,
			"dew_point": 276.54,
			"uvi": 0.5,
			"clouds": 81,
			"visibility": 10000,
			"wind_speed": 4.95,
			"wind_deg": 254,
			"wind_gust": 11.35,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634900400,
			"temp": 283.05,
			"feels_like": 280.63,
			"pressure": 1022,
			"humidity": 66,
			"dew_point": 277.06,
			"uvi": 0.72,
			"clouds": 85,
			"visibility": 10000,
			"wind_speed": 4.95,
			"wind_deg": 253,
			"wind_gust": 11.4,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634904000,
			"temp": 283.99,
			"feels_like": 282.8,
			"pressure": 1022,
			"humidity": 64,
			"dew_point": 277.45,
			"uvi": 0.81,
			"clouds": 88,
			"visibility": 10000,
			"wind_speed": 4.77,
			"wind_deg": 266,
			"wind_gust": 10.94,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634907600,
			"temp": 284.27,
			"feels_like": 283.16,
			"pressure": 1022,
			"humidity": 66,
			"dew_point": 278.05,
			"uvi": 1.21,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 4.76,
			"wind_deg": 270,
			"wind_gust": 10.25,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634911200,
			"temp": 284.66,
			"feels_like": 283.62,
			"pressure": 1022,
			"humidity": 67,
			"dew_point": 278.72,
			"uvi": 0.86,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 4.74,
			"wind_deg": 272,
			"wind_gust": 9.75,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0.04
		},
		{
			"dt": 1634914800,
			"temp": 285.2,
			"feels_like": 284.21,
			"pressure": 1022,
			"humidity": 67,
			"dew_point": 279.32,
			"uvi": 0.45,
			"clouds": 100,
			"visibility": 10000,
			"wind_speed": 4.52,
			"wind_deg": 277,
			"wind_gust": 8.93,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634918400,
			"temp": 284.96,
			"feels_like": 284,
			"pressure": 1022,
			"humidity": 69,
			"dew_point": 279.36,
			"uvi": 0.21,
			"clouds": 94,
			"visibility": 10000,
			"wind_speed": 4.27,
			"wind_deg": 283,
			"wind_gust": 8.81,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634922000,
			"temp": 284.66,
			"feels_like": 283.72,
			"pressure": 1022,
			"humidity": 71,
			"dew_point": 279.5,
			"uvi": 0,
			"clouds": 95,
			"visibility": 10000,
			"wind_speed": 3.68,
			"wind_deg": 284,
			"wind_gust": 8.89,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634925600,
			"temp": 284.36,
			"feels_like": 283.44,
			"pressure": 1023,
			"humidity": 73,
			"dew_point": 279.66,
			"uvi": 0,
			"clouds": 95,
			"visibility": 10000,
			"wind_speed": 3.57,
			"wind_deg": 278,
			"wind_gust": 8.61,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634929200,
			"temp": 283.39,
			"feels_like": 282.48,
			"pressure": 1024,
			"humidity": 77,
			"dew_point": 279.61,
			"uvi": 0,
			"clouds": 34,
			"visibility": 10000,
			"wind_speed": 3.66,
			"wind_deg": 278,
			"wind_gust": 9.35,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634932800,
			"temp": 282.89,
			"feels_like": 281.23,
			"pressure": 1024,
			"humidity": 79,
			"dew_point": 279.49,
			"uvi": 0,
			"clouds": 37,
			"visibility": 10000,
			"wind_speed": 3.21,
			"wind_deg": 277,
			"wind_gust": 8.6,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634936400,
			"temp": 282.42,
			"feels_like": 280.95,
			"pressure": 1025,
			"humidity": 82,
			"dew_point": 279.4,
			"uvi": 0,
			"clouds": 56,
			"visibility": 10000,
			"wind_speed": 2.74,
			"wind_deg": 272,
			"wind_gust": 7.1,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634940000,
			"temp": 282.11,
			"feels_like": 280.76,
			"pressure": 1025,
			"humidity": 83,
			"dew_point": 279.34,
			"uvi": 0,
			"clouds": 67,
			"visibility": 10000,
			"wind_speed": 2.48,
			"wind_deg": 266,
			"wind_gust": 6.55,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634943600,
			"temp": 281.89,
			"feels_like": 280.52,
			"pressure": 1025,
			"humidity": 83,
			"dew_point": 279.18,
			"uvi": 0,
			"clouds": 74,
			"visibility": 10000,
			"wind_speed": 2.44,
			"wind_deg": 266,
			"wind_gust": 6.85,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634947200,
			"temp": 281.46,
			"feels_like": 280.32,
			"pressure": 1025,
			"humidity": 85,
			"dew_point": 279.02,
			"uvi": 0,
			"clouds": 64,
			"visibility": 10000,
			"wind_speed": 2.06,
			"wind_deg": 261,
			"wind_gust": 5.7,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04n"
				}
			],
			"pop": 0
		},
		{
			"dt": 1634950800,
			"temp": 281.18,
			"feels_like": 280.12,
			"pressure": 1025,
			"humidity": 85,
			"dew_point": 278.9,
			"uvi": 0,
			"clouds": 17,
			"visibility": 10000,
			"wind_speed": 1.92,
			"wind_deg": 253,
			"wind_gust": 5.51,
			"weather": [
				{
					"id": 801,
					"main": "Clouds",
					"description": "few clouds",
					"icon": "02n"
				}
			],
			"pop": 0
		}
	],
	"daily": [
		{
			"dt": 1634814000,
			"sunrise": 1634798083,
			"sunset": 1634835312,
			"moonrise": 1634837220,
			"moonset": 1634800440,
			"moon_phase": 0.53,
			"temp": {
				"day": 282.33,
				"min": 279.88,
				"max": 283.88,
				"night": 280.99,
				"eve": 282.76,
				"morn": 282.3
			},
			"feels_like": {
				"day": 279.61,
				"night": 279.03,
				"eve": 281.24,
				"morn": 278.86
			},
			"pressure": 1011,
			"humidity": 55,
			"dew_point": 273.65,
			"wind_speed": 7.89,
			"wind_deg": 281,
			"wind_gust": 15.13,
			"weather": [
				{
					"id": 502,
					"main": "Rain",
					"description": "heavy intensity rain",
					"icon": "10d"
				}
			],
			"clouds": 49,
			"pop": 1,
			"rain": 9.59,
			"uvi": 1.57
		},
		{
			"dt": 1634900400,
			"sunrise": 1634884587,
			"sunset": 1634921589,
			"moonrise": 1634924640,
			"moonset": 1634891160,
			"moon_phase": 0.56,
			"temp": {
				"day": 283.05,
				"min": 280.28,
				"max": 285.2,
				"night": 282.11,
				"eve": 284.66,
				"morn": 280.28
			},
			"feels_like": {
				"day": 280.63,
				"night": 280.76,
				"eve": 283.72,
				"morn": 277.4
			},
			"pressure": 1022,
			"humidity": 66,
			"dew_point": 277.06,
			"wind_speed": 4.95,
			"wind_deg": 254,
			"wind_gust": 12.06,
			"weather": [
				{
					"id": 804,
					"main": "Clouds",
					"description": "overcast clouds",
					"icon": "04d"
				}
			],
			"clouds": 85,
			"pop": 0.04,
			"uvi": 1.21
		},
		{
			"dt": 1634986800,
			"sunrise": 1634971092,
			"sunset": 1635007868,
			"moonrise": 1635012360,
			"moonset": 1634981760,
			"moon_phase": 0.59,
			"temp": {
				"day": 285.24,
				"min": 280.42,
				"max": 286.55,
				"night": 283.38,
				"eve": 284.86,
				"morn": 280.65
			},
			"feels_like": {
				"day": 284.05,
				"night": 282.39,
				"eve": 283.99,
				"morn": 279.92
			},
			"pressure": 1025,
			"humidity": 59,
			"dew_point": 277.46,
			"wind_speed": 3.72,
			"wind_deg": 215,
			"wind_gust": 9.6,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04d"
				}
			],
			"clouds": 55,
			"pop": 0,
			"uvi": 1.68
		},
		{
			"dt": 1635073200,
			"sunrise": 1635057597,
			"sunset": 1635094149,
			"moonrise": 1635100440,
			"moonset": 1635072360,
			"moon_phase": 0.62,
			"temp": {
				"day": 287.55,
				"min": 283.53,
				"max": 287.6,
				"night": 285.6,
				"eve": 286.91,
				"morn": 283.83
			},
			"feels_like": {
				"day": 286.61,
				"night": 285.2,
				"eve": 286.51,
				"morn": 282.78
			},
			"pressure": 1016,
			"humidity": 60,
			"dew_point": 279.78,
			"wind_speed": 5.15,
			"wind_deg": 193,
			"wind_gust": 11.47,
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10d"
				}
			],
			"clouds": 85,
			"pop": 0.2,
			"rain": 0.12,
			"uvi": 0.9
		},
		{
			"dt": 1635159600,
			"sunrise": 1635144102,
			"sunset": 1635180430,
			"moonrise": 1635189000,
			"moonset": 1635162600,
			"moon_phase": 0.65,
			"temp": {
				"day": 288.4,
				"min": 284.04,
				"max": 288.67,
				"night": 284.41,
				"eve": 286.08,
				"morn": 284.04
			},
			"feels_like": {
				"day": 287.68,
				"night": 283.68,
				"eve": 285.28,
				"morn": 283.64
			},
			"pressure": 1016,
			"humidity": 65,
			"dew_point": 281.75,
			"wind_speed": 3.98,
			"wind_deg": 251,
			"wind_gust": 10.58,
			"weather": [
				{
					"id": 802,
					"main": "Clouds",
					"description": "scattered clouds",
					"icon": "03d"
				}
			],
			"clouds": 25,
			"pop": 0.19,
			"uvi": 1.53
		},
		{
			"dt": 1635246000,
			"sunrise": 1635230607,
			"sunset": 1635266713,
			"moonrise": 1635278220,
			"moonset": 1635252420,
			"moon_phase": 0.68,
			"temp": {
				"day": 288.1,
				"min": 281.88,
				"max": 288.45,
				"night": 285.38,
				"eve": 286.18,
				"morn": 281.88
			},
			"feels_like": {
				"day": 287.32,
				"night": 285.06,
				"eve": 285.65,
				"morn": 281.25
			},
			"pressure": 1021,
			"humidity": 64,
			"dew_point": 281.3,
			"wind_speed": 3.94,
			"wind_deg": 229,
			"wind_gust": 10.27,
			"weather": [
				{
					"id": 803,
					"main": "Clouds",
					"description": "broken clouds",
					"icon": "04d"
				}
			],
			"clouds": 61,
			"pop": 0,
			"uvi": 2
		},
		{
			"dt": 1635332400,
			"sunrise": 1635317113,
			"sunset": 1635352997,
			"moonrise": 1635368100,
			"moonset": 1635341640,
			"moon_phase": 0.71,
			"temp": {
				"day": 289.07,
				"min": 285.34,
				"max": 289.07,
				"night": 286.5,
				"eve": 286.87,
				"morn": 287.48
			},
			"feels_like": {
				"day": 288.91,
				"night": 286.4,
				"eve": 286.7,
				"morn": 287.27
			},
			"pressure": 1015,
			"humidity": 84,
			"dew_point": 286.23,
			"wind_speed": 5.19,
			"wind_deg": 222,
			"wind_gust": 12.71,
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10d"
				}
			],
			"clouds": 100,
			"pop": 1,
			"rain": 4.51,
			"uvi": 2
		},
		{
			"dt": 1635418800,
			"sunrise": 1635403619,
			"sunset": 1635439282,
			"moonrise": 1635458520,
			"moonset": 1635430260,
			"moon_phase": 0.75,
			"temp": {
				"day": 288.67,
				"min": 285.22,
				"max": 288.67,
				"night": 287.48,
				"eve": 286.6,
				"morn": 285.22
			},
			"feels_like": {
				"day": 288.11,
				"night": 287.06,
				"eve": 286.22,
				"morn": 285.02
			},
			"pressure": 1015,
			"humidity": 70,
			"dew_point": 283.11,
			"wind_speed": 4.81,
			"wind_deg": 199,
			"wind_gust": 11.52,
			"weather": [
				{
					"id": 500,
					"main": "Rain",
					"description": "light rain",
					"icon": "10d"
				}
			],
			"clouds": 21,
			"pop": 0.63,
			"rain": 1.52,
			"uvi": 2
		}
	]
}