import { APIkey } from "./OpenWeatherMapApiKey.js"

let buttonQuery = document.querySelector('#buttonQuery')
let buttonTestData = document.querySelector('#buttonTestData')
let inputCity = document.querySelector('#inputCity')
let inputLang = document.querySelector('#inputLang')
let panelForecastHourly = document.querySelector('#forecastHourly')
let checkboxBaseURL = document.querySelector('#cbBaseURL')
let radioTimeZoneSelectedCity = document.querySelector('#radioTimeZoneSelectedCity')
let radioGroupTimeZone = document.querySelector('#radioGroupTimeZone')
let divWeatherCity = document.querySelector('#weatherCity')
let checkboxWind = document.querySelector('#cbWind')
let checkboxHumidity = document.querySelector('#cbHumidity')
let buttonScaleMinus = document.querySelector('#buttonScaleMinus')
let buttonScalePlus = document.querySelector('#buttonScalePlus')
let datalistCities = document.querySelector('#сities')
let buttonGetByLocation = document.querySelector('#buttonGetByLocation')



const baseURLRemote = 'https://api.openweathermap.org/data/2.5/'
const baseURLLocal = 'http://localhost:3000/'
let baseURL = baseURLRemote

checkboxBaseURL.onclick = function (event) {
	baseURL = event.target.checked ? baseURLLocal : baseURLRemote
}

//---------------------------------- loadSettings -----------------------------------

function loadSettings() {
	setCity(localStorage.getItem('city'))

	let lang = localStorage.getItem('lang')
	if (lang && lang != 'undefined')
		inputLang.value = lang

	let fontSize = localStorage.getItem('fontSize')
	if (fontSize && fontSize > 0)
		setFontSize(fontSize)

	let сities = localStorage.getItem('сities')
	if (сities)	{
		let сitiesArr = JSON.parse(сities)
		for (let city of сitiesArr)
			addCityToList(city)
	}

}
loadSettings()

//---------------------------------------------------------------------------------

buttonTestData.onclick = buttonTestDataClick
async function buttonTestDataClick() {
	if (inputCity.value == 'Kiev') {
		var {weatherCityJson} = await import('./weatherCityKiev.mjs')
		var {weatherForecastJson} = await import('./weatherForecastKiev.mjs')
	}

	if (inputCity.value == 'London') {
		var {weatherCityJson} = await import('./weatherCityLondon.mjs') // eslint-disable-line no-redeclare
		var {weatherForecastJson} = await import('./weatherForecastLondon.mjs') // eslint-disable-line no-redeclare
	}

	processWeatherCity(weatherCityJson)
	processWeatherForecast(weatherForecastJson, true)
}


buttonQuery.onclick = function() {
	let xhr = new XMLHttpRequest()

	let url = new URL(baseURL + 'weather')
	url.searchParams.set('q', city())

	url.searchParams.set('appid', APIkey)
	url.searchParams.set('lang', lang())

	xhr.open('GET', url)
	xhr.responseType = 'json'

	xhr.onload = requestCityOnload
	xhr.send()
}


let g_weatherCity

function requestCityOnload() {
	if (this.response.cod != 200) {
		alert(this.response.message)
		return
	}

	let weatherCity = this.response
	console.log(weatherCity)

	processWeatherCity(weatherCity)

	queryWeatherForecast(weatherCity.coord.lat, weatherCity.coord.lon)

	// save settings
	if (queriedByCoords) {
		queriedByCoords = false
		setCity(weatherCity.name)
	}

	localStorage.setItem('city', city())
	localStorage.setItem('lang', lang())

	addCityToList(city())

	let datalistCities = document.querySelectorAll('#сities>option')
	let datalistCitiesValues = [...datalistCities].map(el => el.value)
	let сities = JSON.stringify(datalistCitiesValues)
	localStorage.setItem('сities', сities)
}

function processWeatherCity(weatherCity) {
	g_weatherCity = weatherCity
	if (!weatherCity) {
		divWeatherCity.innerText = "-"
		return
	}

	let weatherDescription = weatherCity.weather[0].main
	let time = new Date(weatherCity.dt * 1000)

	let timeZoneStr = getTimeZoneStr()
	let options = {}
	if (timeZoneStr)
		options.timeZone = timeZoneStr

	divWeatherCity.innerText = `Now, ${time.toLocaleString(lang(), options)} (${timeZoneStr}),
	in ${weatherCity.name} is ${normalizeTemp(weatherCity.main.temp)} °C, ${weatherDescription}`
}

function queryWeatherForecast(lat, lon) {
	let xhr = new XMLHttpRequest()
	let url = new URL(baseURL + 'onecall')

	url.searchParams.set('appid', APIkey)
	url.searchParams.set('lat', lat)
	url.searchParams.set('lon', lon)

	xhr.open('GET', url)
	xhr.responseType = 'json'

	xhr.onload = requestForecastOnload
	xhr.send()
}

let g_weatherForecast

function requestForecastOnload() {
	let weatherForecast = this.response
	console.log(weatherForecast)
	processWeatherForecast(weatherForecast, true)
}

function processWeatherForecast(weatherForecast, animate) {
	g_weatherForecast = weatherForecast
	panelForecastHourly.innerHTML = ''
	if (!weatherForecast) return

	let rule = getCSSRule('.forecastCard')
	rule.style.width = ''

	let hourSunrise = new Date(g_weatherCity?.sys.sunrise * 1000).getHours()
	let hourSunset = new Date(g_weatherCity?.sys.sunset * 1000).getHours()
	let prevDayNum = 0
	let hourForecastEls = []

	//rerender since TimeZone might have changed (taken from weatherForecast.timezone)
	processWeatherCity(g_weatherCity)

	let timeZoneStr = getTimeZoneStr()
	let options = {
		day: '2-digit'
	}
	if (timeZoneStr)
		options.timeZone = timeZoneStr

	for (const hourForecast of weatherForecast.hourly) {
		let date = new Date(hourForecast.dt * 1000)
		let dayNum = + date.toLocaleDateString(lang(), options)
		if (dayNum != prevDayNum) {
			prevDayNum = dayNum

			hourForecastEls.push(
				addDateLabel(panelForecastHourly, date, timeZoneStr, animate)
			)
		}
		let isNight = checkNight(hourSunrise, hourSunset, date.getHours())

		hourForecastEls.push(
			addHourForecast(panelForecastHourly, hourForecast, timeZoneStr, isNight, animate)
		)
	}

	if (animate) {
		hourForecastEls.forEach( (el, i) =>
			setTimeout( () => {
				el.classList.remove('hidden')

				if (i == hourForecastEls.length - 1)
					alignForecastEls()
			}, 10 * i)
		)
	} else {
		alignForecastEls()
	}


	function alignForecastEls() {
		let widthOfEls = [...hourForecastEls].map(el => el.offsetWidth)
		// console.log(widthOfEls)
		let maxWidth = Math.max(...widthOfEls)
		// console.log(maxWidth)

		let rule = getCSSRule('.forecastCard')
		rule.style.width = maxWidth/fontSize() + 'em'
	}

}

function checkNight(hourSunrise, hourSunset, hourTest) {
	if (hourSunset > hourSunrise)
		return ((hourTest < hourSunrise) || (hourTest > hourSunset))
	else
		return ((hourTest < hourSunrise) && (hourTest > hourSunset))
}

function addHourForecast(parent, hourForecast, timeZoneStr, isNight, hide) {
	let weatherDescription = hourForecast.weather[0].main

	let date = new Date((hourForecast.dt) * 1000)

	let options = {
		// hour12: false,
		hour: '2-digit',
		minute: '2-digit',
	}

	if (timeZoneStr)
		options.timeZone = timeZoneStr

	let dateStr = date.toLocaleTimeString(lang(), options)

	let night = isNight ? 'night' : ''
	let hidden = hide ? 'hidden' : ''

	let html = /*html*/`
		<div class="forecastCard forecastHour ${night} ${hidden}"  >	
			<div class="Hour">${dateStr}</div>	
			<img src="https://openweathermap.org/img/w/${hourForecast.weather[0].icon}.png">	
			<div class="Description">${weatherDescription}</div>	
			<div class="Temperature">${normalizeTemp(hourForecast.temp)} <span class="unit">°C</span></div>	
			${ checkboxWind.checked ? /*html*/`<div class="Wind">${hourForecast.wind_speed} <span class="unit">m/s</span></div>` : '' }
			${ checkboxHumidity.checked ? /*html*/`<div class="Humidity">${hourForecast.humidity} <span class="unit">%</span></div>` : '' }
		</div>`

	parent.insertAdjacentHTML('beforeend', html)
	let createdElement = parent.lastChild

	let hourForecastAll = JSON.stringify(hourForecast, null, 4)
	createdElement.querySelector('img').title = hourForecastAll

	return createdElement
}

function addDateLabel(parent, date, timeZoneStr, hide) {
	let options = {
		day: '2-digit',
		month: 'short'
	}

	if (timeZoneStr)
		options.timeZone = timeZoneStr

	let formattedDate = date.toLocaleDateString(lang(), options)
	formattedDate = formattedDate.replace(/[.]/g, '')
	let formattedDateStrs = formattedDate.split(' ')

	let hidden = hide ? 'hidden' : ''

	let html = /*html*/`
		<div class="forecastCard dateLabel ${hidden}" >	
			<div class="Hour">${formattedDateStrs.join('<br>')}:</div>	
		</div>`

	parent.insertAdjacentHTML('beforeend', html)
	let createdElement = parent.lastChild
	return createdElement
}

function normalizeTemp(t) {
	const T0 = 273.15
	return Math.round(t - T0)
}

function getCSSRule(selector) {
	for (let rule of document.styleSheets[0].cssRules)
		if (rule.selectorText == selector)
			return rule
}

//--------------------------------------- city ------------------------------------------

function city() {
	return inputCity.value
}

function setCity(value) {
	inputCity.value = value
}

var prevCity
inputCity.onclick = function (event) {
	prevCity = inputCity.value
	inputCity.value = ""
}

inputCity.onchange = function (event) {
	prevCity = null
	localStorage.setItem('city', inputCity.value)
}


inputCity.onblur = function (event) {
	if (prevCity)
		inputCity.value = prevCity
}


function addCityToList(city) {
	let thisCity = document.querySelectorAll(`#сities>option[value="${city}"]`)
	if (thisCity.length > 0) return

	let option = document.createElement('option');
	option.value = city;
	datalistCities.append(option)
}

//------------------------------------- lang -----------------------------------

function lang() {
	return inputLang.value ?? 'en'
}

inputLang.onkeypress = function (event) {
	if (event.code.includes('Enter')) {
		localStorage.setItem('lang', inputLang.value)
	}
}

//-------------------------------- render options ---------------------------------

radioGroupTimeZone.onchange = renderOptionChanged
checkboxWind.onchange = renderOptionChanged
checkboxHumidity.onchange = renderOptionChanged
function renderOptionChanged(event) {
	processWeatherForecast(g_weatherForecast)
}

function getTimeZoneStr() {
	return (radioTimeZoneSelectedCity.checked && g_weatherForecast) ?
		g_weatherForecast.timezone :
		Intl.DateTimeFormat().resolvedOptions().timeZone
}

//------------------------------------ scale --------------------------------------

function fontSize() {
	return panelForecastHourly.computedStyleMap().get('font-size').value
}

function setFontSize(value) {
	panelForecastHourly.attributeStyleMap.set('font-size', CSS.px(value))
}

buttonScaleMinus.onclick = function (event) {
	let newFontSize = fontSize() - 1
	localStorage.setItem('fontSize', newFontSize)
	setFontSize(newFontSize)
}

buttonScalePlus.onclick = function (event) {
	let newFontSize = fontSize() + 1
	localStorage.setItem('fontSize', newFontSize)
	setFontSize(newFontSize)
}

//------------------------------- Get weather by coordinates -------------------------------

buttonGetByLocation.onclick = function() {
	if ('geolocation' in navigator) {
		navigator.geolocation.getCurrentPosition(function (location) {
			console.log('Location : ' + location.coords.latitude + ', ' + location.coords.longitude)
			queryWeatherByLocation(location.coords.latitude, location.coords.longitude)
		});
	} else {
		alert('Geolocation API not supported.')
	}
}

var queriedByCoords = false
function queryWeatherByLocation(lat, lon) {
	let xhr = new XMLHttpRequest()

	let url = new URL(baseURL + 'weather')
	url.searchParams.set('lat', lat)
	url.searchParams.set('lon', lon)

	url.searchParams.set('appid', APIkey)
	url.searchParams.set('lang', lang())

	xhr.open('GET', url)
	xhr.responseType = 'json'

	xhr.onload = requestCityOnload
	xhr.send()

	setCity('')
	queriedByCoords = true
}
