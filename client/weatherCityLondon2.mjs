export const weatherCityLondonJson = 
{
    "coord": {
        "lon": -0.1257,
        "lat": 51.5085
    },
    "weather": [
        {
            "id": 804,
            "main": "Clouds",
            "description": "overcast clouds",
            "icon": "04n"
        }
    ],
    "base": "stations",
    "main": {
        "temp": 282.03,
        "feels_like": 281.71,
        "temp_min": 280.92,
        "temp_max": 283.01,
        "pressure": 995,
        "humidity": 93
    },
    "visibility": 10000,
    "wind": {
        "speed": 1.34,
        "deg": 279,
        "gust": 5.36
    },
    "clouds": {
        "all": 100
    },
    "dt": 1634781830,
    "sys": {
        "type": 2,
        "id": 2019646,
        "country": "GB",
        "sunrise": 1634798083,
        "sunset": 1634835312
    },
    "timezone": 3600,
    "id": 2643743,
    "name": "London",
    "cod": 200
}


