import express from "express"
import mongoose from "mongoose"

mongoose.connect('mongodb://localhost/weather')

import { WeatherCityModel, WeatherForecastModel } from "./models.mjs"

mongoose.connection.on('connected', () => {
	console.log('mongoose connected')
})

var city

async function main() {
	const app = express()
	app.use(express.static('client'))
	app.use(express.json())

	app.get('/weather', async function(req, res) {
		console.log('get /weather', req.query.q)
		city = req.query.q

		const doc = await WeatherCityModel.findOne( {city} )
		var weatherCityJson = doc.JSON

		res.json(weatherCityJson)
	})

	app.get('/onecall', async function(_req, res) {
		console.log('get /onecall')

		const doc = await WeatherForecastModel.findOne( {city} )
		var weatherForecastJson = doc.JSON

		res.json(weatherForecastJson)
	})

	app.post('/addCity', async function(req, res) {
		let city = req.query.city
		console.log(`post /addCity ${city} req.body.name: ${req.body.name}  -----------------------`)
		// console.log(req.body)

		const cityExists = await WeatherCityModel.exists({ city })
		let message
		let status = 200

		if (cityExists)
			message = `${city} already exists in DB`
		else {
			const doc = await WeatherCityModel.create( {
				city,
				JSON: req.body
			} )

			await doc.save()

			message = `Added ${city} to DB`
			status = 201
		}

		res.status(status).send(message)
		console.log(message)
	})

	app.post('/addForecast', async function(req, res) {
		let city = req.query.city
		console.log(`post /addForecast ${city} req.body.timezone: ${req.body.timezone}  ---------------------`)
		// console.log(req.body)

		const cityExists = await WeatherForecastModel.exists({ city })
		let message
		let status = 200

		if (cityExists)
			message = `${city} Forecast already exists in DB`
		else {
			const doc = await WeatherForecastModel.create( {
				city,
				JSON: req.body
			} )

			await doc.save()

			message = `Added ${city} Forecast to DB`
			status = 201
		}

		res.status(status).send(message)
		console.log(message)
	})

	app.listen(3000, function(){
		console.log('listening on 3000')
	})
}

main()

