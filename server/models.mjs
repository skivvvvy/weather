import mongoose from "mongoose"

const WeatherCitySchema = new mongoose.Schema({
	city: {
		type: String,
		required: true,
	},

	JSON: {
		type: mongoose.Mixed,
		required: true
	},
})


const WeatherForecastSchema = new mongoose.Schema({
	city: {
		type: String,
		required: true,
	},

	JSON: {
		type: mongoose.Mixed,
		required: true
	},
})

export let WeatherCityModel = mongoose.model('weather_cities', WeatherCitySchema)
export let WeatherForecastModel = mongoose.model('weather_forecasts', WeatherForecastSchema)
